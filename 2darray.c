//a[i][j] = *(*(a+i)+j)

#include <stdio.h>

int main () {
    int B[2][3];
    int (*p)[3] = B;
    
    printf("%d\n", B);
    printf("%d\n", *B);
    printf("%d\n", p);
}
