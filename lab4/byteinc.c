#include <stdio.h>

union anint {
   unsigned char chars[4];
   unsigned int theint;
};

int main () {
   union anint myint;
   myint.theint = 123456789;
   
   printf("%d\n", myint.theint);
   printf("%u\n", myint.chars[0]);
   printf("%u\n", myint.chars[1]);
   printf("%u\n", myint.chars[2]);
   printf("%u\n", myint.chars[3]);
}
