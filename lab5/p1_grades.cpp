#include <stdio.h>

char GRADE_MAP[] = { 'F', 'F', 'F', 'F', 'F', 'F', 'D', 'C', 'B', 'A', 'A'};

class Grade {
private:
  char letter;
  int percent;

public:
  Grade ();
  ~Grade ();
  void setGradeByLetter(char let);
  void setGradeByPercent(int perc);
  void printGrade();
};

Grade::Grade () {
  letter = 'F';
  percent = 0;
}


void Grade::setGradeByLetter (char let) {
  letter = let;
  percent = 100 - (let - 'A') * 10 - 5;
}

void Grade::setGradeByPercent (int perc) {
  percent = perc;
  letter = GRADE_MAP[perc / 10];
}

void Grade::printGrade () {
  printf("Grade %d: %c\n", percent, letter);
}

int main () {
  Grade* g = new Grade();

}
