#include <stdio.h>
#include <stdlib.h>

void PrintHelloWolrd() {
    printf("Hello World\n");
}
int *Add(int *a, int *b) {
    int c = *a + *b;
    return &c; // not okay, only if global or heap section
    // int *c = (int*)malloc(sizeof(int));
    // *c = *a + *b;
}

int main() {
    int a = 2, b = 4;
    int* ptr = Add(&a, &b);
    //PrintHelloWolrd();
    printf("Sum = %d\n", *ptr);
}
